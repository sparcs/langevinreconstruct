
% Augmented Dickey-Fuller test of stationarity

% Note: The Dickey-Fuller test has different variants depending on the form
% of alternative hypothesis. The 3 variants being considered in mtlab are
% 1) Autoregressive model variant (use comma-separated pairs 'model','AR')
% 2) Autoregressive model with drift (use comma-separated pairs 'model','ARD')
% 3) Trend stationary model variant (use comma-separated pairs 'model','TS')


 % Outputs:
% The output h = 1 indicates rejection of the unit-root null in favor of the alternative model (means that data are stationary).
% The output h = 0 indicates failure to reject the unit-root null.
% Pvalue : gives the Pvalue.
% Statistics : Gives us the corresponding value of the Dickey-Fuller test.

clc;
% Phycocyanin data
data=csvread('New_Cyanobacteria.csv');
% The third parameter in the adftest is lag. The bigger it is the more
% Autoregressive we have. First we consider an array of lags and then pick
% the one whose BIC is minimum (for climate data appropriate lag =8).
[h,~,~,~,reg]=adftest(data,'model','ARD','lags',0:20);    

% The following tells us how many lags we need (the one whose BIC is minimum)
A=zeros(1,20);
for i=1:20
    A(i)=reg(i).BIC;
end
lag=find(min(A)==A);
% Now, we again apply the adftest with the appropriate lag we just found,
% here appropriate lag=8
[h_Phycocyanin Pvalue_Phycocyanin,Statistics_Phycocyanin]=adftest(data,'model','TS','lags',lag)


% Climate data
% for the climate data the appropriate lag=24.
data=csvread('Cadata.csv');data=data(data>-100 & data<100);data=fliplr(data); %This is to remove a few NANs. 
[h,~,~,~,reg]=adftest(data,'model','ARD','lags',0:50);
A=zeros(1,50);
for i=1:50
    A(i)=reg(i).BIC;
end
lag=find(min(A)==A);
[h_Climate Pvalue_Climate,Statistics_Climate]=adftest(data,'model','TS','lags',lag)
