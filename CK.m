function [p_31, p_ck, C] = CK(data, L, R, bins, bins2, TimeScale)
    % Function for visualization of the Chapman-Kolmogorov equation.
    %Inputs:
    %   data      the data set (evenly sampled time series
    %   L and R   are, respectively, the lower and upper limit considered for the states being spanned by the range
    %             of data. Normally, L=min(data) and R=max(data) are good choices but for limited data one might consider a slightly
    %             bigger value for L and a slightly smaller value for R.
    %   bins      number of bins for the p_31. Should be carefully chosen depending on the size of data. This number should not be so big so that
    %             there would be little data in each bin and it should not be so small so that we are left with few bins. In the following citation, it is recommended to choose
    %             bins in such a way that each bin will, at least, contain 100 observations).
    %   bins2     number of bins for p_ck (can higher than bins, interpolation is used).
    %   TimeScale   a timescale under which conditional probability probability p(x3,t3|x1,t1) is calculated 1) directly and, 2) by the Chapman-Kolmogorov equation. If these two
    %               probabilities are close under TimeScale = 1 this would be the sign of Markov property in data

    %Output:
    %   p_31     conditional probability p(x3,t3|x1,t1) which is calculated by the data dirfectly
    %   p_ck     conditional probability p(x3,t3|x1,t1) which is estimated by the Chapman-Kolmogorov equation 
    %                        
    %                            p(x3,t3|x1,t1)=integral(p(x3,t3|x2,t2)p(x2,t2|x1,t1) dx2)
    %
    %            where the range of integral is over all intermediate states x2 corresponding with all intermediate times
    %            t1<t2<t3. For stationary processes and for the simplicity, we set t1=0,t2=t3/2,t3=2,4,6,... . 
 
    % Note:      Although validity of Chapman-Kolmogorov equation is a necessary condition for the Markov property but it is considered to be a
    %            strong indication for the Markov property. 
 
 
    % The material for this method can be found in the following reference
    % Tabar, R. (2019). Analysis and data-based reconstruction of complex
    % nonlinear dynamical systems, Springer.
 
 

    data = data(data >= L & data <= R); % limiting the data to lower and upper bounds L and R.
    I = find(data == max(data));
    data(I) = data(I) - 10^(-6) * range(data); % Pushing, relative to the range of data, the biggest data inside the last bin
    x = linspace(L, R, bins + 1); % x is the bins borders.
    dx = x(2) - x(1);
    C = x + dx / 2;
    C = C(1:end - 1); % C is the bin centers.
   % [N, ~] = histcounts(data, x);

    T2 = TimeScale;
    T3 = 2 * TimeScale;

    y = linspace(C(1), C(end), bins2);
    P_x3x1 = zeros(length(y), bins);
    % P(x3,t3|x1,t1)
    for i = 1:bins
        Idx = find(data(1:end - T3) >= C(i) - dx / 2 & data(1:end - T3) < C(i) + dx / 2);
        data_future = data(Idx + T3);
        [A, ~] = ksdensity(data_future, y);
        P_x3x1(:, i) = A ./ trapz(y, A);
    end

    % P(x3,t3|x2,t2)
    P_x3x2 = zeros(length(y), bins);
    for i = 1:bins
        Idx = find(data(1:end - T2) >= C(i) - dx / 2 & data(1:end - T2) < C(i) + dx / 2);
        data_future = data(Idx + T2);
        [A, ~] = ksdensity(data_future, y);
        P_x3x2(:, i) = A ./ trapz(y, A);
    end

    % P_CK(x3,t3|x1,t1)
    [Xq, Yq] = meshgrid(y, y);
    X = repmat(C, [bins2 1]);
    Y = repmat(y', [1 bins]);
    p_32 = interp2(X, Y, P_x3x2, Xq, Yq, 'cubic');

    p_ck = zeros(bins2, bins2); % p_ck is p(x3,t3|x1,t1) being estimated using the Chapman-Kolmogoroc equation
    for i = 1:bins2
        p1_32 = p_32;
        for j = 1:bins2
            p_ck(i, j) = trapz(y, p1_32(i, :) .* p1_32(:, j)');
        end
    end
    % To correct for small numerical inacuracies (note: it can be shown that p_ck(x3,t3|x1,t1) is a probability density for any x1)
    for j = 1:bins2
        p_ck(:, j) = p_ck(:, j) ./ trapz(y, p_ck(:, j));
    end

    p_31 = interp2(X, Y, P_x3x1, Xq, Yq, 'cubic'); % p_31 is p(x3,t3|x1,t1) being calculated by data directly
end


