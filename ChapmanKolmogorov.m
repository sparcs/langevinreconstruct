function X2 = ChapmanKolmogorov(data, L, R, bins, MaxTime)
    %   X2 = ChapmanKolmogorov(data, L, R, bins, MaxTime)
    %   Chapman Kolmogorov test for the Markov property
    %
    %Inputs:
    %   data      the data set (evenly sampled time series
    %   L and R   are, respectively, the lower and upper limit considered for the states being spanned by the range
    %             of data. Normally, L=min(data) and R=max(data) are good choices but for limited data one might consider a slightly
    %             bigger value for L and a slightly smaller value for R, to aviod bins with few data.
    %   bins      number of bins for the reconstruction Should be carefully chosen depending on the size of data. This number should not be so big so that
    %             there would be little data in each bin and it should not be so small so that we are left with few bins. In the following citation, it is recommended to choose
    %             bins in such a way that each bin will, at least, contain 100 observations).
    %   MaxTime   Maximum time scale we investigate the Markov property (for the purpose of this test it is better to choose an even number)   
    %
    %Output:
    %   X2       A distance measure between the conditional probability p(x3,t3|x1,t1) and the one being calculated by intermediate states x2
    %            at time t2 (t3>t2>t1) using the Chapman-Kolmogorov equation integral(p(x3,t3|x2,t2)*p(x2,t2|x1,t1) dx2). For stationary processes
    %            and for the ease of calculations we set t1=0,t2=t3/2 and vary t3. Then we plot X2 as a function of t2. Markov property is best
    %            fulfilled for the time scale t2 which minimizes the distance X2.
    %
    % Note:      Although validity of Chapman-Kolmogorov equation is a necessary condition for the Markov property but it is considered to be a
    %            strong indication for the Markov property. 


    data = data(data >= L & data <= R); % limiting the data to lower and upper bounds L and R.
    I = find(data == max(data));
    data(I) = data(I) - 10^(-6) * range(data); % Pushing, relative to the range of data, the biggest data inside the last bin
    x = linspace(L, R, bins + 1); % x is the bins borders.
    dx = x(2) - x(1);
    C = x + dx / 2;
    C = C(1:end - 1); % C is the bin centers.

    p = TransPro_LastState(data, bins, x, MaxTime);
    t1 = 0;
    X2 = zeros(1, MaxTime / 2);
    for t3 = 2:2:MaxTime
        t2 = t3 / 2;
        p1 = p(:, :, t3);
        p1_CK = zeros(bins, bins); %p1 being calculated using Chapman-Kolmogorov equation
        for i = 1:bins
            for j = 1:bins
                ptimesp = p(:, j, t3 - t2)' .* p(i, :, t2 - t1);
                ndx = ~isnan(ptimesp);
                p1_CK(i, j) = trapz(C(ndx), ptimesp(ndx));
            end
        end
        F = (p1 - p1_CK).^2 ./ (p1 + p1_CK);
        F(isnan(F)) = 0;
        X2(t2) = trapz(C, trapz(C, F, 2));
    end
end


%SUBROUTINES
%**********************************************************************************************************************************
function p = TransPro_LastState(data, bins, x, MaxTime)
    p = zeros(bins, bins, MaxTime);
    for t = 1:MaxTime
        for i = 1:bins
            Idx = find(data(1:end - t) >= x(i) & data(1:end - t) < x(i + 1)); % We cannot consider the last t data because we need them for prediction
            data_future = data(Idx + t);
            [N, ~] = histcounts(data_future, x);
            p(i, :, t) = N ./ sum(N);
        end
    end
end


