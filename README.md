**LangevinReconstruct**

This folder contains the matlab code for the analysis of the following paper:\
Arani, B.M.S., Carpenter, S.R., Lahti, L., van Nes, E.H. & Scheffer, M. (2021) Exit time as a measure of ecological resilience Science 372 (6547): eaay4895 [[fulltext]](http://dx.doi.org/10.1126/science.aay4895)


_Dependency:_\
Chebfun https://github.com/chebfun/chebfun/.\
Parallel Computing Toolbox (optional) - Mathworks\
Curve Fitting Toolbox - Mathworks\
Econometrics Toolbox - Mathworks (needed for the Augmented Dickey-Fuller test only)


_Functions:_\
ChapmanKolmogorov.m - This function is used to calculate the Einstein-Markov time scale\
CK.m                  - This function is calculates the conditional probabilities using the Chapman Kolmogorov equation (as test for the Markov property)\
DickeyFuller.m        - Augmented Dickey-Fuller test of stationarity (needs econometrics toolbox)\
Langevin_bootstrap.m  - implements several bootstrap approaches.\
langevin_eq.m         - class that can analyse and plot the fitted Langevin equation\
LangevinReconst.m     - the Langevin Approach for reconstructing the Langevin equation from data.


 

_Contacts:_\
Latest version of the code is available on git: \
https://git.wageningenur.nl/sparcs/langevinreconstruct\
Babak M.S. Arani, Wageningen University (currently Groningen University), The Netherlands (m.shojaeiarani@gmail.com)\
Egbert van Nes, Wageningen University, The Netherlands (Egbert.vannes@wur.nl)
